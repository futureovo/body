public class Person {
    private String firstName;
    private String lastName;
   

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName()+"'s Name: " +
                 firstName +
                " " + lastName ;
    }
}
public class Student extends Person{
    private int studentId;
    
    public Student(String firstName, String lastName, int studentId) {
        super(firstName, lastName);
        this.studentId = studentId;
    }
 

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    @Override
    public String toString() {
        return super.toString() +
                ", Id: " + studentId ;
    }
}
public class StudentView {
    public void printStudentDetails(Student student)
    {
       
        System.out.println(student);
    }
}
public class StudentController {
    private Student model;
    private StudentView view;

    public StudentController(Student model, StudentView view)
    {
        this.model = model;
        this.view = view;
    }

    public void setStudentName(String fname, String lname)
    {
        model.setFirstName(fname);
        model.setLastName(lname);
    }

    public String getStudentName()
    {
        return model.getFirstName()+" "+model.getLastName();
    }

    public void setStudentID(int id)
    {
        model.setStudentId(id);
    }

    public int getStudentId()
    {
        return model.getStudentId();
    }

    public void updateView()
    {
        view.printStudentDetails(model);
    }
}
public class StudentMVCTest {
    public static void main(String[] args) {
        Student model  = getStudentFromDatabase();

        StudentView view = new StudentView();

        StudentController controller = new StudentController(model, view);

        controller.updateView();

        controller.setStudentName("Michael","Jordan");
        controller.setStudentID(999);

        controller.updateView();
    }
    private static Student getStudentFromDatabase()
    {
        Student student = new Student("Future","Hendrix",777);
        return student;
    }
}